package com.fjnavarro.psp.test2.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;

public class Servidor {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// LinkedList donde almacenar los clientes conectados y pudiendo controlar la sincronizaci�n
		LinkedList<Thread> listaHilos = new LinkedList<Thread>();
		
		// TODO Auto-generated method stub
		try {
			// Creamos el socket Servidor
	        ServerSocket sk = new ServerSocket(8083);
	        			
			// Ponemos el proceso en un bucle infinito para aceptar peticiones
			while (true) {
				// Activamos el socket para aceptar peticiones de clientes
		        Socket cliente = sk.accept();
		        
		        // Creamos un hilo por cada cliente
		        Runnable NCl = new HiloClienteEnServer(cliente);

		        // Iniciamos el hilo y controlamos la sincronizaci�n
		        synchronized (listaHilos)
		        { 
		        	listaHilos.add(new Thread(NCl));
		        	listaHilos.get(listaHilos.size()-1).start();
		        }
		        
		        // mostramos el n�mero de clientes conectados
		        System.out.println("Conectados "+listaHilos.size()+" clientes.");
			  }
	   } catch (IOException e) {
	          System.out.println(e);
	   }
	}

}
