package com.fjnavarro.psp.test2.client;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.Semaphore;

public class HiloCliente implements Runnable {
	private String nombre;
	private int id;
	private int nMensaje;
	private boolean presentado;
	private String mensaje;
	// preparamos la t�cnica del sem�foro para los hilos
	final Semaphore semaphore = new Semaphore (1);
	
	/* Con el constructor incializamos la clase */
	public HiloCliente(int id){
		// asignamos los datos b�sicos del cliente
		this.id=id;
		this.nombre="test"+id;
		// tenemos un flag para saber si nos hemos presentado al server
		this.presentado=false;
		// ponemos un n�mero de mensajes al azar - lo he limitado a 10 mensajes para no alargar mucho los test
		this.nMensaje=new Double(Math.random() * 10).intValue();
	}
	
	/* M�todo principal de una clase Runnable */
	public void run(){
        try{
        	// si no nos hemos presentado al server, lo hacemos, de esta forma el server crea su hilo y almacena nuestro nombre
	        if(!this.presentado){
	        	this.mensaje="Hola soy "+nombre;
	        	this.enviarMensaje();
	        	this.presentado=true;
	        }
	        
        	// enviamos mensajes al servidor mientras que no hayamos terminado con el n�mero de mensajes del hilo actual
        	while(this.nMensaje>0){
		        // definimos la pregunta a enviar
        		this.mensaje=nombre+" - Pregunta n�mero "+this.nMensaje;
		        
		        // enviamos el mensaje al server
		        this.enviarMensaje();
		        
		        // quitamos un mensaje de los pendientes a enviar
		        this.nMensaje--;
        	}
        	
        	// el hilo a termiando su ejecuci�n nos despedimos del server, para que el server pueda cerrar el hilo
	        this.mensaje=nombre+" - Hasta la vista baby";        
	        
	        // enviamos el mensaje al server
	        this.enviarMensaje();	        
        } catch (Exception e) {
        	e.printStackTrace();
	   }
	}
	
	/* M�todo que se encarga de conectar con el server y enviar las preguntas */
	public void enviarMensaje(){
		try{
			// inicio del sem�foro en la secci�n cr�tica
			semaphore.acquire();
			
			// Mostramos el hilo actual y el mensaje a enviar al server - as� controlamos es estado de las comunicaciones desde la consola
	        System.out.println(Thread.currentThread()+"---"+this.mensaje);
	        
	        // Creamos el socket cliente
			Socket sk = new Socket("127.0.0.1", 8083);
			
			// Definos los Stream para gestionar el flujo de salida de datos
			OutputStream out = sk.getOutputStream();
			PrintWriter pr = new PrintWriter(out, true);
			
			// Enviamos el texto al servidor
			pr.println(this.mensaje);
			
			// Cerramos la conexi�n al socket
			sk.close();
			
	        // ponemos a dormir el hilo durante un tiempo aleatorio, de esta forma se va lanzado las conexiones de forma aleatoria
	        Thread.sleep(new Double(Math.random() * 10000).intValue());
	        
	        // fin del sem�foro en la secci�n cr�tica
	        semaphore.release();	        
		}catch(InterruptedException e){
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
