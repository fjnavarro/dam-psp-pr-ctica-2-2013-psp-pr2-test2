package com.fjnavarro.psp.test2.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.concurrent.Semaphore;

public class HiloClienteEnServer implements Runnable {
	private Socket cliente;
	private String nombreCliente="";
	// preparamos la t�cnica del sem�foro para los hilos
	final Semaphore semaphore = new Semaphore (1);
	
	/* Con el constructor incializamos la clase */
	public HiloClienteEnServer(Socket cliente){
		this.cliente=cliente;
	}
	
	/* M�todo de una clase Runnable */
	public void run(){
		try{
			// inicio del sem�foro en la secci�n cr�tica
			semaphore.acquire();
			
			// obtenemos el mensaje
			this.obtenerMensaje();

	        // cerramos el socket
			this.cliente.close();
			
			// fin del sem�foro en la secci�n cr�tica
	        semaphore.release();
		}catch(InterruptedException e){
			System.out.println(e);
		}catch (Exception e) {
	          System.out.println(e);
	   }
	}
	
	/*
	 * Obtenemos el mensaje que nos env�a el cliente
	 * */
	public void obtenerMensaje(){
		String datos=null;
		BufferedReader entrada1;
		String respuesta="";
		String pregunta="";
		
		try {
	        // Definos los Stream para gestionar el flujo de entrada de datos
			entrada1 = new BufferedReader(
	        new InputStreamReader(this.cliente.getInputStream()));
	        
	        // Obtenemos el String que nos env�a el cliente
	        datos = entrada1.readLine();
	        
	        // obtenemos el nombre del cliente
	        if(datos.indexOf("Hola soy ")!=-1){
	        	// si el mensaje es del saludo inicial
	        	
	        	// obtenemos el nombre del cliente
	        	// Cogemos el tama�o de "Hola soy " y le quitamos uno para obtener la posici�n inicial del titulo
	        	this.nombreCliente = datos.substring(8);
	        	
	        	// preparamos la respuesta
	        	respuesta = "Encantado "+this.nombreCliente;
	        }else if(datos.indexOf(" - Pregunta n�mero ")!=-1){
	        	// si es el mensaje es una pregunta
	        	
	        	// obtenemos el nombre del cliente
	        	this.nombreCliente = datos.substring(0,datos.indexOf(" - Pregunta n�mero "));
	        	
	        	// obtenemos la pregunta
	        	// para coger la posici�n donde hacer el corte, cogemos el tama�o del nombre y le sumamos el separador " - "
	        	pregunta= datos.substring(this.nombreCliente.length()+3);
	        	
	        	// preparamos la respuesta
	        	respuesta = "Ok '"+pregunta+"' "+this.nombreCliente;
	        }else if(datos.indexOf(" - Hasta la vista baby")!=-1){
	        	// si el mensaje es el de despiedida
	        	
	        	// obtenemos el nombre del cliente
	        	this.nombreCliente = datos.substring(0,datos.indexOf(" - Hasta la vista baby"));
	        	
	        	// preparamos la respuesta
	        	respuesta = "Ciao "+this.nombreCliente;
	        }

	        // mostramos la conversaci�n	        
	        System.out.println("____");
	        System.out.println("Mensaje del cliente: "+datos);
	        System.out.println("Respuesta del servidor: "+respuesta);
	        System.out.println("____\n\n");
	        
	        // cerramos los Stream
	        entrada1.close();
	   } catch (IOException e) {
	          System.out.println(e);
	   } catch (Exception e) {
	          System.out.println(e);
	   }
	}
}
