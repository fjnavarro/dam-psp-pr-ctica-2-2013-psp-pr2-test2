package com.fjnavarro.psp.test2.client;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int nClientes;
		int contador=1;
		Thread[] listaHilos;
		
		// TODO Auto-generated method stub
		try{
			Menu menu = new Menu();
			
			// mostramos el t�tulo de la aplicaci�n y un mensaje solicitando el n�mero de clientes
			menu.mostrarMenu();
			
			// obtenemos el n�mero de clientes que queremos para realizar el test contra el servidor
			nClientes = menu.getNClientes();
			
			// inicializamos el array que contendr� la lista de hilos con el n�mero de clientes solicitado
			listaHilos = new Thread[nClientes];
			
			do{
				while(contador<=nClientes){
					// Creamos un hilo por cada cliente
			        Runnable NCl = new HiloCliente(contador);
			        listaHilos[contador-1] = new Thread(NCl);
			        
			        // Iniciamos el hilo
			        listaHilos[contador-1].start();
			        
			        // incrementamos el contador
			        contador++;
				}
				
				// Chequeamos que no haya ningun hilo en funcionamiento
				boolean hiloActivo=false;
				for(int i=0;i<nClientes;i++){
					// recorremos la lista de hilos comprobando uno a uno que no est�n vivos
					if(listaHilos[i].isAlive()){
						hiloActivo=true;
					}
				}
				// si tenemos todos los hilos finalizados
				if(!hiloActivo){
					// salimos del bucle y de la aplicaci�n
					break;
				}
			}while(true);
		}catch(Exception e){
			e.printStackTrace();
		}
		// finaliza la aplicaci�n
		System.out.println("****F�N DE LA APLICACI�N****");
	}

}
