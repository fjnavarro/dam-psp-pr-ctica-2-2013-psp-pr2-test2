# Aplicación en Java - psp-pr2-test2 - Servidor con un protocolo y test clientes #

*Comparto el siguiente código realizado en el Técnico Superior de Desarrollo de Aplicaciones Multiplataforma.

*Se realizó como una práctica de la asignatura de “Programación de Servicios y Procesos”, es una aplicación Java.

*Hay que tener en cuenta que sólo se pudo utilizar las tecnologías y técnicas dadas en el trimestre de la práctica.

*La práctica se realizó el 08-06-2013

===================


## Texto de la práctica ##

Durante el segundo trimestre hemos visto cómo se gestionan los hilos y como trabajar con comunicaciones. 

Realizaremos un servidor que habla cierto protocolo y un test que actuará como cliente. Como mínimo usaremos un protocolo como el especificado, similar a saludos de cortesía:

El cliente inicia la conversación presentándose, por ejemplo “hola soy test1”, a lo que el servidor responderá “encantado test1”, almacenando el nombre test1 como identificador del cliente.

El servidor responderá a cada pregunta “pregunta” del cliente con “ok 'pregunta' test1”

Del mismo modo existirá un comando de despedida.

El funcionamiento del servidor será el siguiente: 

1. Esperará que se produzcan conexiones.

2. Cada conexión entrante creará un hilo que le atenderá, mostrando por consola el desarrollo de la conversación.

3. Se mostrará información sobre el número de clientes conectados.

4. Este proceso se repetirá hasta que pulsemos la letra ‘S’ de salir.

El funcionamiento del cliente será el siguiente: 

1. Esperará que se introduzcan por teclado el número de clientes que se van a lanzar.

2. Se creará tantos hilos como clientes se van a lanzar, cada uno con un nombre distinto.

3. Con tiempos aleatorios se lanzarán conexiones al servidor con un número aleatorios de mensajes.

4. Se mostrarán por consola el estado de las comunicaciones.

## Nota ##

En el caso de la aplicación propuesta es necesario que tenga una comunicación fiable, por ello he elegido el protocolo TCP.

## Screenshots ##

Servidor funcionando

[![Servidor funcionando](http://fjnavarro.com/assets/screenshot-bitbucket/psp-pr2-test2/Captura1.png)](http://fjnavarro.com/assets/screenshot-bitbucket/psp-pr2-test2/Captura1.png)

Aplicación test funcionando

[![Aplicación test funcionando](http://fjnavarro.com/assets/screenshot-bitbucket/psp-pr2-test2/Captura2.png)](http://fjnavarro.com/assets/screenshot-bitbucket/psp-pr2-test2/Captura2.png)

## Autor ##

* Francisco José Navarro García <fran@fjnavarro.com>
* Twitter : *[@fjnavarro_](https://twitter.com/fjnavarro_)*
* Linkedin: *[https://www.linkedin.com/in/fjnavarrogarcia](https://www.linkedin.com/in/fjnavarrogarcia)*
* Blog    : *[http://www.fjnavarro.com/](http://www.fjnavarro.com/)*