package com.fjnavarro.psp.test2.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Clase que gestiona el Men� principal de la aplicaci�n
 */
public class Menu {
	/**
	 * Muestra el men� principal de la aplicaci�n
	 */
	public void mostrarMenu(){
		// mostrar cabecera del men�
		System.out.println("**********************");
		System.out.println("** Lanzador de Test **");
		System.out.println("**********************\n\n");
		
		System.out.println("**********************************************************************************");
		System.out.println("Escriba el n�mero de clientes que quiere ejecutar contra el servidor y pulse ENTER");
		System.out.println("**********************************************************************************");
	}
	
	/**
	 * Obtiene el n�mero de veces que un usuario quiere repetir un test en concreto
	 */
	public int getNClientes(){
		int repetir = 0;
		
		// objetos para obtener los caracteres introducidos desde el teclado
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader leer = new BufferedReader(isr);

		try{
			// obtenemos la cadena introducida desde el teclado y la transformamos en un n�mero
			repetir = Integer.parseInt(leer.readLine());
		}catch(IOException e){
			e.printStackTrace();
		}
		
		return repetir;
	}
}
